//
// Created by vargat on 2019. 12. 05..
//
#include <stdio.h>
#include <stdlib.h>
#include "date.h"
#include "util.h"
#include "register.h"

#define DELIMITER '.'

const int monthDays[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

date_validity_code convertDateStringToStruct(const char *date, dateStruct *dateStruct1) {
    char currentChar;
    int charCount = 0, delimiterCount = 0, i = 0;
    dateStruct tempDate;
    date_validity_code tempDateValCode;

    if (!date) {
        return E_NULL_POINTER;
    }
    tempDate.year = 0;
    tempDate.month = 0;
    tempDate.day = 0;

    while ((currentChar = *(date + i++)) != '\0') {
        charCount++;
        if ((currentChar >= '0' && currentChar <= '9') || currentChar == DELIMITER) {
            if (currentChar == DELIMITER && *(date + i) != DELIMITER) {
                delimiterCount++;
                charCount = 0;
            } else {
                switch (delimiterCount) {
                    case 0:
                        if (charCount < 4 && *(date + i) == DELIMITER) {
                            return E_INVALID_DATE_FORMAT;
                        } else if (charCount <= 4) {
                            tempDate.year += intPow(10, 4 - charCount) * (currentChar - 48);
                        } else {
                            return E_INVALID_DATE_FORMAT;
                        }
                        break;
                    case 1:
                        if (charCount == 1 && *(date + i) == DELIMITER) {
                            tempDate.month += intPow(10, 0) * (currentChar - 48);
                        } else if (charCount <= 2) {
                            tempDate.month += intPow(10, 2 - charCount) * (currentChar - 48);
                        } else {
                            return E_INVALID_DATE_FORMAT;
                        }
                        break;
                    case 2:
                        if (charCount == 1 && *(date + i) == DELIMITER) {
                            tempDate.day += intPow(10, 0) * (currentChar - 48);
                        } else if (charCount <= 2) {
                            tempDate.day += intPow(10, 2 - charCount) * (currentChar - 48);
                        } else {
                            return E_INVALID_DATE_FORMAT;
                        }
                        break;
                    case 3:
                        if (charCount > 0) {
                            return E_INVALID_DATE_FORMAT;
                        }
                        break;
                    default:
                        return E_TOO_MANY_DELIMITER;
                }
            }
        } else {
            return E_ILLEGAL_CHARACTER;
        }
    }
    if (delimiterCount < 3) {
        return E_INVALID_DATE_FORMAT;
    } else {

        tempDateValCode = dateCheck(tempDate);
        if (tempDateValCode == 0) {
            dateStruct1->year = tempDate.year;
            dateStruct1->month = tempDate.month;
            dateStruct1->day = tempDate.day;
            return tempDateValCode;
        } else return tempDateValCode;
    }
}

date_validity_code dateCheck(dateStruct dateToCheck) {
    if (dateToCheck.year < DATE_MIN_YEAR) {
        return E_YEAR_MIN_ERROR;
    }

    if (dateToCheck.month < 1 || dateToCheck.month > 12) {
        return E_MONTH_VALUE_ERROR;
    }

    switch (dateToCheck.month) {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            if (dateToCheck.day < 1 || dateToCheck.day > 31)
                return E_DAY_VALUE_ERROR;
            break;
        case 4:
        case 6:
        case 9:
        case 11:
            if (dateToCheck.day < 1 || dateToCheck.day > 30)
                return E_DAY_VALUE_ERROR;
            break;
        case 2:
            if (dateToCheck.day < 1 || dateToCheck.day > 28 + leap_year_check(dateToCheck.year))
                return E_DAY_VALUE_ERROR;
            break;
    }
    return DATE_VALID;
}

int leap_year_check(int yearToCheck) {
    if ((yearToCheck % 4 == 0 && yearToCheck % 100 != 0) || yearToCheck % 400 == 0) {
        return 1;
    } else
        return 0;
}

int daysFromZero(dateStruct dateToCalculate) {
    int days = 0;
    int leapYears = 0;
    int i = 0;
    int tempYear = dateToCalculate.month >= 2 ? dateToCalculate.year : dateToCalculate.year - 1;

    leapYears = tempYear / 4 - tempYear / 100 + tempYear / 400;

    days = dateToCalculate.year * 365 + leapYears + dateToCalculate.day;

    for (; i < dateToCalculate.month; i++) {
        days += monthDays[i];
    }
    return days;
}

char *date_validity_string(date_validity_code code) {
    char *message = (char *) malloc(DATE_MESSAGE_MAX_LENGTH);

    switch (code) {
        case DATE_VALID:
            message = "Date is valid";
            break;
        case E_ILLEGAL_CHARACTER:
            message = "Not valid: Date contains illegal character";
            break;
        case E_TOO_MANY_DELIMITER:
            message = "Not valid: Date contains too many delimiters";
            break;
        case E_INVALID_DATE_FORMAT:
            message = "Invalid date format!";
            break;
        case E_YEAR_MIN_ERROR:
            sprintf(message, "Not valid: Year of the date is less than the minimum (%d)", DATE_MIN_YEAR);
            break;
        case E_MONTH_VALUE_ERROR:
            message = "Not valid: Month value must be between 1 and 12";
            break;
        case E_DAY_VALUE_ERROR:
            message = "Not valid: The value of the day does not match the given month";
            break;
        case E_CURRENT_DATE_ERROR:
            message = "Not valid: The date cannot be later than current date";
            break;
        case E_NULL_POINTER:
            message = "Not valid: Null pointer";
            break;
        default:
            message = "Unknown error";
    }
    return message;
}

date_validity_code requestDate(char *message, dateStruct *requestedInput) {
    dateStruct convertedInput;
    char *dateInput = (char *) malloc(DIM_DATE);
    date_validity_code errorCode;

    printf("%s", message);
    while (1) {
        if (getStringInput(NULL, DIM_DATE, dateInput)) {
            errorCode = convertDateStringToStruct(dateInput, &convertedInput);
            if (!errorCode) {
                break;
            } else {
                printf("%s\n", date_validity_string(errorCode));
                printf("Try again (format: yyyy.mm.dd. - length: %d): ", DIM_DATE);
                continue;
            }
        } else {
            printf("Too long string, try again (format: yyyy.mm.dd. - length: %d): ", DIM_DATE);
        }
    }
    *requestedInput = convertedInput;
    free(dateInput);
    return errorCode;
}

char *convertDateStructToString(dateStruct dateStructToConvert) {
    char *returnDate = (char *) malloc(DIM_DATE);
    sprintf(returnDate, "%04d.%02d.%02d.", dateStructToConvert.year, dateStructToConvert.month,
            dateStructToConvert.day);
    return returnDate;
}