//
// Created by vargat on 2019. 12. 05..
//

#ifndef BLOOD_DONATION_REGISTER_DATE_H
#define BLOOD_DONATION_REGISTER_DATE_H

#define DATE_MIN_YEAR 1900
#define DATE_MESSAGE_MAX_LENGTH 64

typedef enum {
    DATE_VALID = 0,
    E_ILLEGAL_CHARACTER,
    E_TOO_MANY_DELIMITER,
    E_INVALID_DATE_FORMAT,
    E_YEAR_MIN_ERROR,
    E_MONTH_VALUE_ERROR,
    E_DAY_VALUE_ERROR,
    E_CURRENT_DATE_ERROR,
    E_UNKNOWN_DATE_ERROR,
    E_NULL_POINTER = 99

} date_validity_code;

typedef struct {
    int year;
    int month;
    int day;
} dateStruct;

date_validity_code convertDateStringToStruct(const char *, dateStruct *);

date_validity_code dateCheck(dateStruct);

int leap_year_check(int);

char *date_validity_string(date_validity_code);

int daysFromZero(dateStruct);

date_validity_code requestDate(char *message, dateStruct *requestedInput);

char *convertDateStructToString(dateStruct);

#endif //BLOOD_DONATION_REGISTER_DATE_H
