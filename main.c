#include <stdlib.h>
#include <libgen.h>
#include <stdio.h>
#include "util.h"
#include "register.h"
#include "menu.h"
#include "date.h"


int process(char *programName, char *fileName) {

    int numberOfRecords = 0;
    donorRecord *donorRegister;
    dateStruct currentDate;
    clrscr();
    // welcome message
    printf(ANSI_BCOLOR_BLACK"WELCOME TO THE BLOOD REGISTER PROGRAM!\n\n"ANSI_COLOR_RESET);

    if (requestDate("Please enter today's date (format: yyyy.mm.dd.), then press <ENTER>: ", &currentDate)) {
        return EXIT_FAILURE;
    } else {
        clrscr();
    }

    if (!countLinesInRegister(fileName, &numberOfRecords)) {
        printf("The %s file, as argument of %s program has been opened\n", fileName, programName);
        printf("The number of registered donors has been counted: %d\n", numberOfRecords);

    } else {
        printf(ANSI_COLOR_RED"Can't open file: %s\n" ANSI_COLOR_RESET, fileName);
        return EXIT_FAILURE;
    }

    donorRegister = (donorRecord *) malloc((numberOfRecords) * sizeof(donorRecord));

    if (donorRegister) {
        printf("The memory allocation for the donorRecord register succeeded!\n");
    } else {
        printf("The memory allocation process has failed!");
        return EXIT_FAILURE;
    }
    if (!readRegister(fileName, donorRegister, numberOfRecords, currentDate)) {
        printf("The content of the %s file has been read into the struct array\n", fileName);

        donorRegister = selectMenu(donorRegister, &numberOfRecords, currentDate);

        if (isConfirmed("Do you want to save the database to a file?\n"))
            writeRegister(fileName, donorRegister, numberOfRecords);

        free(donorRegister);
        exitProgram();

    } else {
        printf("The reading from the %s file has failed!\n", fileName);
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        usage(basename(argv[0]));
        return EXIT_FAILURE;
    } else
        return process(basename(argv[0]), argv[1]);
}