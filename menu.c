//
// Created by vargat on 2019. 12. 09.
//
#include <stdio.h>
#include "menu.h"
#include "util.h"

void displayMenu() {
    char *menuMessage =
            ANSI_COLOR_MAGENTA
            "\nPlease choose from the following menu items by entering numbers: \n"
            ANSI_COLOR_RESET
            "\t1 - View MemberList\n"
            "\t2 - Add new Member(s)\n"
            "\t3 - Modify member data\n"
            "\t4 - Delete record\n"
            "\t5 - Searching for a specific blood type, requesting blood donation via email\n"
            "\t6 - Statistics\n"
            ANSI_COLOR_GREEN
            "\te - Exit program\n"
            ANSI_COLOR_RESET;
    printf("%s", menuMessage);
}

donorRecord *selectMenu(donorRecord *donorRegister, int *numberOfRecords, dateStruct currentDate) {
    char choice;
    displayMenu();
    do {

        choice = getCharInput("The number of your choice: ");
        switch (choice) {
            case '1':
                clrscr();
                printRegister(donorRegister, *numberOfRecords);
                if (isConfirmed("Would you like to continue? ")) {
                    clrscr();
                    displayMenu();
                    continue;
                } else {
                    return donorRegister;
                }

            case '2':
                clrscr();
                printf("Enter new donorRecord:\n\n");
                do {
                    donorRegister = addNewDonorToRegister(donorRegister, &numberOfRecords, currentDate);
                } while (isConfirmed("\nAnother donorRecord? \n"));

                if (isConfirmed("Would you like to continue? ")) {
                    clrscr();
                    displayMenu();
                    continue;
                } else {
                    return donorRegister;
                }

            case '3':
                clrscr();
                modifyDonorRecord(donorRegister, *numberOfRecords, currentDate);
                if (isConfirmed("Would you like to continue? ")) {
                    clrscr();
                    displayMenu();
                    continue;
                } else {
                    return donorRegister;
                }

            case '4':
                clrscr();
                donorRegister = deleteDonorRecord(donorRegister, &numberOfRecords);
                if (isConfirmed("Would you like to continue? ")) {
                    clrscr();
                    displayMenu();
                    continue;
                } else {
                    return donorRegister;
                }

            case '5':
                clrscr();
                searchDonorByBloodType(donorRegister, *numberOfRecords, currentDate);
                if (isConfirmed("Would you like to continue? ")) {
                    clrscr();
                    displayMenu();
                    continue;
                } else {
                    return donorRegister;
                }

            case '6':
                clrscr();
                printStatistics(donorRegister, *numberOfRecords, currentDate);
                if (isConfirmed("Would you like to continue? ")) {
                    clrscr();
                    displayMenu();
                    continue;
                } else {
                    return donorRegister;
                }

            case 'e':
                if (!isConfirmed("Are you sure? ")) {
                    clrscr();
                    displayMenu();
                    continue;
                } else {
                    return donorRegister;
                }

            default:
                printf(ANSI_COLOR_RED "Wrong choice.Enter Again!\n" ANSI_COLOR_RESET);
                break;
        }
    } while (1);
}

