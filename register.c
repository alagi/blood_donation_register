//
// Created by vargat on 2019. 12. 09..
//
#include <stdio.h>
#include <stdlib.h>
#include "register.h"
#include "util.h"
#include "email.h"

const char *READ_ONLY = "r";
const char *WRITE = "w";

const char *bloodTypeNames[DIM_BLOOD_TYPES] = {"A+", "A-", "B+", "B-", "0+", "0-", "AB+", "AB-", "N/A"};

int getName(char *externalMessage, char *name) {
    while (!getStringInput(externalMessage, DIM_NAME, name))
        printf("Name too long, max: %d char!\n", DIM_NAME);
    return EXIT_SUCCESS;
}

int getBloodType(char *externalMessage, char *bloodTypeString, bloodType *bloodTypeConv) {
    while (1) {
        while (!getStringInput(externalMessage, DIM_BLOOD_TYPE_STRING, bloodTypeString))
            printf("Blood Type too long, max: %d char!\n", DIM_BLOOD_TYPE_STRING);

        if ((*bloodTypeConv = convertBloodTypeNameToType(bloodTypeString)) < type_unknown) {
            break;
        } else
            printf("Wrong Blood Type, try again!\n");
    }
    return EXIT_SUCCESS;
}

int getNumberOfDonation(char *externalMessage, int *numberOfDonation) {
    int tempNoD;
    while (1) {
        tempNoD = getIntInput(externalMessage);
        if (tempNoD < 0) {
            printf("Can't be less than 0!\n");
        } else if (tempNoD > MAX_NUMBER_OF_DONATION) {
            printf("Can't be more than %d!\n", MAX_NUMBER_OF_DONATION);
        } else {
            break;
        }
    }
    *numberOfDonation = tempNoD;
    return EXIT_SUCCESS;
}

int getEmail(char *externalMessage, char *donorEmail, email_validity_code *isCorrupted) {
    while (1) {
        while (!getStringInput(externalMessage, DIM_EMAIL, donorEmail))
            printf("Email too long, max: %d char!\n", DIM_EMAIL);

        if (!(*isCorrupted = email_checking(donorEmail))) {
            break;
        } else
            printf("%s\n", email_validity_string(*isCorrupted));
    }
    return EXIT_SUCCESS;
}

int getDateOfLastDonation(
        char *externalMessage, dateStruct *dateOfLastDonation, char *dateOfLastDonationString,
        date_validity_code *isCorrupted, dateStruct currentDate) {


    while (1) {
        *isCorrupted = requestDate(externalMessage, dateOfLastDonation);
        if (!*isCorrupted) {
            if (daysFromZero(*dateOfLastDonation) > daysFromZero(currentDate)) {
                printf("The date you entered cannot be later than today's date: %s\n",
                       convertDateStructToString(currentDate));
                continue;
            } else {
                sscanf(convertDateStructToString(*dateOfLastDonation), "%s",
                       dateOfLastDonationString);
                *isCorrupted = DATE_VALID;
                break;
            }
        }
    }
    return EXIT_SUCCESS;
}

donorRecord *addNewDonorToRegister(donorRecord *donorRegister, int **updatedNumberOfRecords,
                                   const dateStruct currentDate) {

    donorRecord tempDonor;
    donorRecord *tempRegister;
    (**updatedNumberOfRecords)++;

    tempDonor.id = **updatedNumberOfRecords;

    tempDonor.isCorruptedDoD = E_UNKNOWN_DATE_ERROR;
    tempDonor.isCorruptedEmail = E_UNKNOWN_EMAIL_ERROR;

    getName("Last Name: ", tempDonor.last_name);
    getName("First Name: ", tempDonor.first_name);

    getBloodType("Bloood type: ", tempDonor.bloodTypeString, &tempDonor.bloodType);

    getEmail("Email: ", tempDonor.email, &tempDonor.isCorruptedEmail);

    getNumberOfDonation("Number of donations: ", &tempDonor.numberOfDonation);

    if (tempDonor.numberOfDonation == 0) {
        (tempDonor.structDateOfLastDonation).year = 0;
        (tempDonor.structDateOfLastDonation).month = 0;
        (tempDonor.structDateOfLastDonation).day = 0;
        sscanf(convertDateStructToString(tempDonor.structDateOfLastDonation), "%s", tempDonor.dateOfLastDonation);
        tempDonor.isCorruptedDoD = 0;
        printf("Date of last donation is set to min: %s\n", tempDonor.dateOfLastDonation);
    } else {

        getDateOfLastDonation(
                "Date of last donation: ",
                &tempDonor.structDateOfLastDonation, tempDonor.dateOfLastDonation, &tempDonor.isCorruptedDoD,
                currentDate);
    }
    if (tempDonor.bloodType == type_unknown || tempDonor.isCorruptedEmail || tempDonor.isCorruptedDoD) {
        printf("Corrupted record, will not be saved!");
        (**updatedNumberOfRecords)--;
    } else {

        //realloc donorRecord*
        tempRegister = (donorRecord *) realloc(donorRegister, **updatedNumberOfRecords * sizeof(donorRecord));

        if (!tempRegister)
            return donorRegister;
        else
            donorRegister = tempRegister;

        //put data into donorRegister
        (donorRegister + **updatedNumberOfRecords - 1)->id = tempDonor.id;
        sscanf(tempDonor.last_name, "%s", (donorRegister + **updatedNumberOfRecords - 1)->last_name);
        sscanf(tempDonor.first_name, "%s", (donorRegister + **updatedNumberOfRecords - 1)->first_name);
        sscanf(tempDonor.bloodTypeString, "%s", (donorRegister + **updatedNumberOfRecords - 1)->bloodTypeString);
        sscanf(tempDonor.email, "%s", (donorRegister + **updatedNumberOfRecords - 1)->email);
        (donorRegister + **updatedNumberOfRecords - 1)->numberOfDonation = tempDonor.numberOfDonation;
        sscanf(tempDonor.dateOfLastDonation, "%s",
               (donorRegister + **updatedNumberOfRecords - 1)->dateOfLastDonation);
        (donorRegister + **updatedNumberOfRecords -
         1)->structDateOfLastDonation = tempDonor.structDateOfLastDonation;
        (donorRegister + **updatedNumberOfRecords -
         1)->bloodType = tempDonor.bloodType;
        (donorRegister + **updatedNumberOfRecords - 1)->isCorruptedDoD = DATE_VALID;
        (donorRegister + **updatedNumberOfRecords - 1)->isCorruptedEmail = EMAIL_VALID;
        //return updated donorRegister
        return donorRegister;
    }
    //return original donorRegister
    return donorRegister;
}

int searchDonorByBloodType(donorRecord *donorRegister, const int numberOfRecords, const dateStruct currentDate) {
    int i = 0;
    char *bloodTypeString = (char *) malloc(DIM_BLOOD_TYPE_STRING);
    char *restriction = NULL;
    bloodType tempBloodType;
    int countAllowed = 0;

    getBloodType("Which blood type is needed? ", bloodTypeString, &tempBloodType);

    printRegisterHeader("AVAILABLE DONORS:");
    for (; i < numberOfRecords; i++) {
        if (tempBloodType == (donorRegister + i)->bloodType) {
            if ((daysFromZero(currentDate) - daysFromZero((donorRegister + i)->structDateOfLastDonation) <
                 DAYS_TO_WAIT_BEFORE_DONATIONS) ||
                ((donorRegister + i)->isCorruptedDoD || ((donorRegister + i)->isCorruptedEmail)) ||
                ((donorRegister + i)->numberOfDonation == 0 && (donorRegister + i)->structDateOfLastDonation.year > 0)
                    ) {
                restriction = "donation not allowed";
            } else {
                restriction = NULL;
            }
            printRecord(*(donorRegister + i), restriction);
            if (!restriction) {
                countAllowed++;
                (donorRegister + i)->numberOfDonation++;
                (donorRegister + i)->structDateOfLastDonation = currentDate;
                sscanf(convertDateStructToString(currentDate), "%s", (donorRegister + i)->dateOfLastDonation);
            }
        }
    }
    printRegisterFooter();

    if (countAllowed) {
        printf("Invitation emails have been sent to %d donorRecord(s), number of donations has been increased and\n"
               "last date of donations have been updated in the register!\n\n", countAllowed);
    } else {
        printf("Sorry, but found 0 available donors :(\n\n");
    }
    return EXIT_SUCCESS;
}

int countLinesInRegister(const char *fileName, int *numberOfRecords) {
    FILE *file = fopen(fileName, READ_ONLY);
    int c;
    int countCharInLine = 0, countLines = 0;

    if (!file) {
        return EXIT_FAILURE;
    }
    for (c = fgetc(file); c != EOF; c = fgetc(file)) {
        countCharInLine++;
        if (c == '\n') {
            countLines++;
            countCharInLine = 0;
        }
    }
    fclose(file);

    *numberOfRecords = countCharInLine ? ++countLines : countLines;
    return EXIT_SUCCESS;
}

int readRegister(const char *fileName, donorRecord *donorRegister, int numberOfRecords, const dateStruct currentDate) {
    int i = 0;
    FILE *file = fopen(fileName, READ_ONLY);
    if (!file) {
        printf("Can't open file: %s", fileName);
        return EXIT_FAILURE;
    }

    for (; i < numberOfRecords; i++) {

        fscanf(file, "%d %s %s %s %s %d %s",
               &(donorRegister + i)->id,
               (donorRegister + i)->last_name,
               (donorRegister + i)->first_name,
               (donorRegister + i)->bloodTypeString,
               (donorRegister + i)->email,
               &(donorRegister + i)->numberOfDonation,
               (donorRegister + i)->dateOfLastDonation);

        (donorRegister + i)->bloodType = convertBloodTypeNameToType((donorRegister + i)->bloodTypeString);

        (donorRegister + i)->isCorruptedDoD =
                convertDateStringToStruct((donorRegister + i)->dateOfLastDonation,
                                          &(donorRegister + i)->structDateOfLastDonation);


        if (!(donorRegister + i)->isCorruptedDoD &&
            (daysFromZero((donorRegister + i)->structDateOfLastDonation) > daysFromZero(currentDate)))
            (donorRegister + i)->isCorruptedDoD = E_CURRENT_DATE_ERROR;

        if ((donorRegister + i)->isCorruptedDoD) {
            (donorRegister + i)->structDateOfLastDonation.year = 0;
            (donorRegister + i)->structDateOfLastDonation.month = 0;
            (donorRegister + i)->structDateOfLastDonation.day = 0;
        }

        if ((donorRegister + i)->numberOfDonation == 0 && ((donorRegister + i)->structDateOfLastDonation.year == 0)) {
            (donorRegister + i)->isCorruptedDoD = DATE_VALID;
        }

        (donorRegister + i)->isCorruptedEmail = email_checking((donorRegister + i)->email);
    }
    fclose(file);
    return EXIT_SUCCESS;
}

void printRegisterHeader(char *title) {
    int i = 0;
    int lineDim =
            DIM_NAME * 2 + 4 * DIM_BLOOD_TYPE_STRING + DIM_EMAIL + 2 * DIM_DATE + DIM_ID + DIM_NUMBER_OF_DONATIONS;

    printf(ANSI_BCOLOR_BLACK"%s\n"ANSI_COLOR_RESET, title);
    for (i = 0; i < lineDim; i++) {
        printf("_");
    }
    printf("\n");
    printf("%-*s", DIM_ID, "ID");
    printf("%-*s", DIM_NAME, "LAST NAME");
    printf("%-*s", DIM_NAME, "FIRST NAME");
    printf("%-*s", 4 * DIM_BLOOD_TYPE_STRING, "BLOOD TYPE");
    printf("%-*s", DIM_EMAIL, "EMAIL");
    printf("%-*s", DIM_NUMBER_OF_DONATIONS, "NUMBER OF DONATIONS");
    printf("%-*s\n", 2 * DIM_DATE, "LAST DATE OF DONATION");
    for (i = 0; i < lineDim; i++) {
        printf("_");
    }
    printf("\n");
}

void printRegisterFooter() {
    int i = 0;
    int lineDim =
            DIM_NAME * 2 + 4 * DIM_BLOOD_TYPE_STRING + DIM_EMAIL + 2 * DIM_DATE + DIM_ID + DIM_NUMBER_OF_DONATIONS;

    for (i = 0; i < lineDim; i++) {
        printf("_");
    }
    printf("\n\n");
}

void printRegister(donorRecord *donorRegister, const int numberOfRecords) {
    int i = 0;

    printRegisterHeader("LIST OF REGISTERED BLOOD DONORS:");
    for (; i < numberOfRecords; i++) {
        printRecord(*(donorRegister + i), NULL);
    }
    printRegisterFooter();
}

void printRecord(donorRecord donor, char *restriction) {

    printf("%-*d", DIM_ID, donor.id);
    printf("%-*s", DIM_NAME, donor.last_name);
    printf("%-*s", DIM_NAME, donor.first_name);
    if (donor.bloodType == type_unknown)
        printf(ANSI_REDBGN"%-*s"ANSI_COLOR_RESET, 4 * DIM_BLOOD_TYPE_STRING, donor.bloodTypeString);
    else
        printf("%-*s", 4 * DIM_BLOOD_TYPE_STRING, donor.bloodTypeString);

    if (donor.isCorruptedEmail)
        printf(ANSI_REDBGN"%-*s"ANSI_COLOR_RESET, DIM_EMAIL, donor.email);
    else
        printf("%-*s", DIM_EMAIL, donor.email);

    if (donor.numberOfDonation < 0 || donor.numberOfDonation > MAX_NUMBER_OF_DONATION ||
        ((donor.numberOfDonation == 0) && (donor.structDateOfLastDonation.year != 0)))
        printf(ANSI_REDBGN"%-*d"ANSI_COLOR_RESET, DIM_NUMBER_OF_DONATIONS, donor.numberOfDonation);
    else
        printf("%-*d", DIM_NUMBER_OF_DONATIONS, donor.numberOfDonation);

    if (donor.isCorruptedDoD)
        printf(ANSI_REDBGN"%-*s"ANSI_COLOR_RESET, 2 * DIM_DATE, donor.dateOfLastDonation);
    else
        printf("%-*s", 2 * DIM_DATE, donor.dateOfLastDonation);

    if (restriction == NULL)
        printf("\n");
    else
        printf("%-*s\n", DIM_RESTRICTION, restriction);

    //print error line
    if (donor.isCorruptedDoD || donor.isCorruptedEmail || donor.bloodType == type_unknown ||
        donor.numberOfDonation < 0 || ((donor.numberOfDonation == 0) && (donor.structDateOfLastDonation.year != 0))) {
        printf(ANSI_COLOR_RED"| ");
        if (donor.isCorruptedDoD)
            printf("Date: %s | ", date_validity_string(donor.isCorruptedDoD));
        if (donor.isCorruptedEmail)
            printf("Email: %s | ", email_validity_string(donor.isCorruptedEmail));
        if (donor.bloodType == type_unknown)
            printf("Blood type: %s | ", bloodTypeNames[type_unknown]);
        if (donor.numberOfDonation < 0 || ((donor.numberOfDonation == 0) && (donor.structDateOfLastDonation.year != 0)))
            printf("N.of don is invalid | ");
        printf("\n"ANSI_COLOR_RESET);
    }
}

int writeRegister(const char *fileName, donorRecord *donorRegister, const int numberOfRecords) {
    int i = 0;
    FILE *file = fopen(fileName, WRITE);

    if (!file) {
        printf("Can't write file: %s", fileName);
        return EXIT_FAILURE;
    }

    for (; i < numberOfRecords; i++) {
        fprintf(file, "%-*d", DIM_ID, (donorRegister + i)->id),
                fprintf(file, "%-*s", DIM_NAME, (donorRegister + i)->last_name);
        fprintf(file, "%-*s", DIM_NAME, (donorRegister + i)->first_name);
        fprintf(file, "%-*s", 4 * DIM_BLOOD_TYPE_STRING, (donorRegister + i)->bloodTypeString);
        fprintf(file, "%-*s", DIM_EMAIL, (donorRegister + i)->email);
        fprintf(file, "%-*d", DIM_NUMBER_OF_DONATIONS, (donorRegister + i)->numberOfDonation);
        fprintf(file, "%-*s\n", 2 * DIM_DATE, (donorRegister + i)->dateOfLastDonation);
    }

    if (fprintf != 0)
        printf("contents to file written successfully !\n");
    else
        printf("error writing file !\n");

    fclose(file);
    return EXIT_SUCCESS;
}

bloodType convertBloodTypeNameToType(const char *bloodTypeString) {
    int i = type_A_p;
    for (; i <= type_AB_m; i++) {
        if (!strCompT(bloodTypeString, bloodTypeNames[i]))
            return i;
    }
    return type_unknown;
}

void collectStatistics(const donorRecord *donorRegister, const int numberOfRecords, const dateStruct currentDate,
                       int *counterArrayAllowed, int *counterArrayCountDonorsByBloodType, int *corrEmail,
                       int *corrDate, int *corrNoD, int *bestDonatorID) {

    int i;
    int maxNoD = 0;

    //reset counters
    for (i = 0; i < DIM_BLOOD_TYPES; i++) {
        *(counterArrayAllowed + i) = 0;
        *(counterArrayCountDonorsByBloodType + i) = 0;
    }

    for (i = 0; i < numberOfRecords; i++) {

        (*(counterArrayCountDonorsByBloodType + (donorRegister + i)->bloodType))++;


        if ((daysFromZero(currentDate) - daysFromZero((donorRegister + i)->structDateOfLastDonation) >
             DAYS_TO_WAIT_BEFORE_DONATIONS) &&
            !((donorRegister + i)->isCorruptedDoD) &&
            ((donorRegister + i)->bloodType != type_unknown) &&
            !((donorRegister + i)->isCorruptedEmail) &&
            ((donorRegister + i)->numberOfDonation > 0 || (donorRegister + i)->structDateOfLastDonation.year == 0))
            (*(counterArrayAllowed + (donorRegister + i)->bloodType))++;

        if ((donorRegister + i)->isCorruptedDoD)
            (*corrDate)++;

        if ((donorRegister + i)->isCorruptedEmail)
            (*corrEmail)++;

        if ((donorRegister + i)->numberOfDonation < 0 ||
            (donorRegister + i)->numberOfDonation > MAX_NUMBER_OF_DONATION ||
            ((donorRegister + i)->numberOfDonation == 0 && (donorRegister + i)->structDateOfLastDonation.year > 0))
            (*corrNoD)++;

        if ((donorRegister + i)->numberOfDonation > maxNoD) {
            maxNoD = (donorRegister + i)->numberOfDonation;
            *bestDonatorID = i;
        }
    }
}

void printStatistics(const donorRecord *donorRegister, const int numberOfRecords, const dateStruct currentDate) {
    int i = 0;
    int *arrayPtrCountDonors = (int *) malloc(numberOfRecords * sizeof(int));
    int *arrayPtrCountAllowedDonors = (int *) malloc(numberOfRecords * sizeof(int));
    int corrEmail = 0, corrDate = 0, corrNoD = 0;
    int bestDonatorId;

    collectStatistics(donorRegister, numberOfRecords, currentDate, arrayPtrCountAllowedDonors, arrayPtrCountDonors,
                      &corrEmail, &corrDate, &corrNoD, &bestDonatorId);
    printf(
            "************************\n"
            "STATISTICS - %s\n"
            "************************\n",
            convertDateStructToString(currentDate));

    printf("Donor count: %d\n", numberOfRecords);
    for (; i < DIM_BLOOD_TYPES; i++) {
        printf("%s :"
               ANSI_COLOR_BBLU"\t%d"ANSI_COLOR_RESET
               " - %d of which are allowed\n", bloodTypeNames[i], *(arrayPtrCountDonors + i),
               *(arrayPtrCountAllowedDonors + i));
    }
    printf("Errors: "
           ANSI_COLOR_RED"%d"ANSI_COLOR_RESET
           " (email), "
           ANSI_COLOR_RED"%d"ANSI_COLOR_RESET
           " (date), "
           ANSI_COLOR_RED"%d"ANSI_COLOR_RESET
           " (nOfDon), "
           ANSI_COLOR_RED"%d"ANSI_COLOR_RESET
           " (bloodType)\n\n", corrEmail, corrDate, corrNoD, *(arrayPtrCountDonors + type_unknown));

    printRegisterHeader("BEST DONATOR: ");
    printf(ANSI_COLOR_GREEN"");
    printRecord(*(donorRegister + bestDonatorId), NULL);
    printf(ANSI_COLOR_RESET"");
    printRegisterFooter();

    free(arrayPtrCountAllowedDonors);
    free(arrayPtrCountDonors);
}

void displayModMenu() {
    char *menuMessage =
            ANSI_COLOR_MAGENTA
            "Fields to modify: \n"
            ANSI_COLOR_RESET
            "\t1 - Last name\n"
            "\t2 - First name\n"
            "\t3 - Blood type\n"
            "\t4 - Email\n"
            "\t5 - Number of Donations\n"
            "\t6 - Date of last donation\n"
            ANSI_COLOR_GREEN
            "\t0 - Save&Exit\n"
            ANSI_COLOR_RED
            "\te - Exit without save\n"
            ANSI_COLOR_RESET
            "The number of your choice: ";
    printf("%s", menuMessage);
}

int modifyName(char *externalMessage, char *structName) {
    char *tempName = (char *) malloc(DIM_NAME);
    printf("Current %s %s\n", externalMessage, structName);
    if (!getName("New: ", tempName)) {
        sprintf(structName, "%s", tempName);
        free(tempName);
        return EXIT_SUCCESS;
    } else {
        return EXIT_FAILURE;
    }
}

int modifyBloodType(char *externalMessage, char *structBloodTypeString, bloodType *structBloodType) {
    char *tempBloodTypeString = (char *) malloc(DIM_BLOOD_TYPE_STRING);
    bloodType tempBloodType;
    printf("Current %s %s\n", externalMessage, structBloodTypeString);
    if (!getBloodType("New blood type: ", tempBloodTypeString, &tempBloodType)) {
        sprintf(structBloodTypeString, "%s", tempBloodTypeString);
        *structBloodType = tempBloodType;
        free(tempBloodTypeString);
        return EXIT_SUCCESS;
    } else {
        return EXIT_FAILURE;
    }
}

int modifyNumberOfDonations(char *externalMessage, int *structNumberOfDonations) {
    int tempNumberOfDonation;
    printf("Current %s %d\n", externalMessage, *structNumberOfDonations);
    if (!getNumberOfDonation("New: ", &tempNumberOfDonation)) {
        *structNumberOfDonations = tempNumberOfDonation;
        return EXIT_SUCCESS;
    } else {
        return EXIT_FAILURE;
    }
}

int modifyEmail(char *externalMessage, char *structEmail, email_validity_code *structIsCorrupted) {
    char *tempEmail = (char *) malloc(DIM_EMAIL);
    email_validity_code tempIsCorrupted = E_UNKNOWN_EMAIL_ERROR;
    printf("Current %s %s\n", externalMessage, structEmail);
    if (!getEmail("New email: ", tempEmail, &tempIsCorrupted)) {
        sprintf(structEmail, "%s", tempEmail);
        *structIsCorrupted = tempIsCorrupted;
        free(tempEmail);
        return EXIT_SUCCESS;
    } else {
        return EXIT_FAILURE;
    }
}

int modifyDateOfLastDonation(char *externalMessage, dateStruct *structDateOfLastDonation,
                             char *structDateOfLastDonationString,
                             date_validity_code *structIsCorrupted, dateStruct currentDate) {

    char *tempDateString = (char *) malloc(DIM_DATE);
    dateStruct tempDate;
    date_validity_code tempIsCorrupted = E_UNKNOWN_DATE_ERROR;
    printf("Current %s %s\n", externalMessage, structDateOfLastDonationString);

    if (!getDateOfLastDonation("New date: ", &tempDate, tempDateString, &tempIsCorrupted, currentDate)) {
        sprintf(structDateOfLastDonationString, "%s", tempDateString);
        *structDateOfLastDonation = tempDate;
        *structIsCorrupted = tempIsCorrupted;
        free(tempDateString);
        return EXIT_SUCCESS;
    } else {
        return EXIT_FAILURE;
    }
}

int modifyDonorRecord(donorRecord *donorRegister, const int numberOfRecords, const dateStruct currentDate) {
    printRegister(donorRegister, numberOfRecords);
    int selectedRecord;
    char choice;
    donorRecord *tempRecord = (donorRecord *) malloc(sizeof(donorRecord));
    while (1) {
        selectedRecord = getIntInput("Select record ID to modify (or 0 to cancel):");
        if (selectedRecord <= numberOfRecords && selectedRecord >= 0)
            break;
        else
            printf("Wrong ID, please choose from above!");
    }
    if (selectedRecord == 0)
        return EXIT_SUCCESS;



    *tempRecord = *(donorRegister + selectedRecord - 1);

    while (1) {
        clrscr();
        printRegisterHeader("RECORD TO MODIFY:");
        printRecord(*tempRecord, NULL);
        printRegisterFooter();
        displayModMenu();

        do {

            choice = getCharInput("The number of your choice: ");

            switch (choice) {
                case '1':
                    if (!modifyName("Last name: ", tempRecord->last_name))
                        printf("Last name has changed to %s\n", tempRecord->last_name);
                    break;

                case '2':
                    if (!modifyName("First name: ", tempRecord->first_name))
                        printf("First name has changed to %s\n", tempRecord->first_name);
                    break;

                case '3':
                    if (!modifyBloodType("Blood type: ", tempRecord->bloodTypeString, &tempRecord->bloodType))
                        printf("Blood type has changed to %s\n", tempRecord->bloodTypeString);
                    break;

                case '4':
                    if (!modifyEmail("Email: ", tempRecord->email, &tempRecord->isCorruptedEmail))
                        printf("Email has changed to %s\n", tempRecord->email);
                    break;

                case '5':
                    if (!modifyNumberOfDonations("Number of donations: ", &tempRecord->numberOfDonation))
                        printf("Number of donations has changed to %d\n", tempRecord->numberOfDonation);
                    if (tempRecord->numberOfDonation == 0) {
                        (tempRecord->structDateOfLastDonation).year = 0;
                        (tempRecord->structDateOfLastDonation).month = 0;
                        (tempRecord->structDateOfLastDonation).day = 0;
                        sscanf(convertDateStructToString(tempRecord->structDateOfLastDonation), "%s",
                               tempRecord->dateOfLastDonation);
                        tempRecord->isCorruptedDoD = 0;
                        printf("Date of last donation is set to default: %s", tempRecord->dateOfLastDonation);
                    }
                    if (tempRecord->numberOfDonation > 0 &&
                        ((tempRecord->structDateOfLastDonation).year < DATE_MIN_YEAR) &&
                        tempRecord->isCorruptedDoD == 0)
                        tempRecord->isCorruptedDoD = E_YEAR_MIN_ERROR;
                    break;

                case '6':
                    if (tempRecord->numberOfDonation == 0) {
                        printf("Please, first modify the \"Number of Donation\" field!\n");
                        if (!modifyNumberOfDonations("Number of donations: ", &tempRecord->numberOfDonation))
                            printf("Number of donations has changed to %d\n", tempRecord->numberOfDonation);
                    }
                    if (!modifyDateOfLastDonation("Date of last donation: ", &tempRecord->structDateOfLastDonation,
                                                  tempRecord->dateOfLastDonation, &tempRecord->isCorruptedDoD,
                                                  currentDate))
                        printf("Date of last donation has changed to %d\n", *tempRecord->dateOfLastDonation);
                    break;

                case '0':
                    printf("Record has saved!\n");
                    *(donorRegister + selectedRecord - 1) = *tempRecord;
                    free(tempRecord);
                    return EXIT_SUCCESS;
                case 'e':
                    if (isConfirmed("Are you sure? ")) {
                        printf("No change in record!\n");
                    } else {
                        break;
                    }
                    free(tempRecord);
                    return EXIT_SUCCESS;
                default:
                    clrscr();
                    printf(ANSI_COLOR_RED "Wrong choice.Enter Again!\n" ANSI_COLOR_RESET);
                    break;
            }
        } while (!choice);

    }


}

donorRecord *deleteDonorRecord(donorRecord *donorRegister, int **updatedNumberOfRecords) {
    donorRecord *tempRegister;
    int selectedRecord;
    int i, counter = 0;
    printRegister(donorRegister, **updatedNumberOfRecords);

    while (1) {
        selectedRecord = getIntInput("Enter record ID to delete or 0 to cancel:");
        if (selectedRecord <= **updatedNumberOfRecords && selectedRecord >= 0)
            break;
        else
            printf("Wrong ID, please choose from above!");
    }
    if (selectedRecord == 0)
        return donorRegister;



    if (isConfirmed("Are you sure? ")) {
        if (isConfirmed("Are you totally sure?")) {
            (donorRegister + selectedRecord - 1)->id = -1;
            printf("Record deleted:\n");
            printRegisterHeader("DELETED RECORD:");
            printRecord(*(donorRegister + selectedRecord - 1), "");
            printRegisterFooter();

            for (i = 0; i < (**updatedNumberOfRecords); i++) {
                if (((donorRegister + i)->id) != -1) {
                    (donorRegister + i)->id = counter + 1;
                    *(donorRegister + counter) = *(donorRegister + i);
                    counter++;
                }
            }
            (**updatedNumberOfRecords)--;

            //realloc donorRecord*
            tempRegister = (donorRecord *) realloc(donorRegister, **updatedNumberOfRecords * sizeof(donorRecord));

            if (!tempRegister)
                return donorRegister;
            else
                donorRegister = tempRegister;
        }
    }
    return donorRegister;
}

