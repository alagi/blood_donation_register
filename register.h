//
// Created by vargat on 2019. 12. 09..
//

#ifndef BLOOD_DONATION_REGISTER_REGISTER_H
#define BLOOD_DONATION_REGISTER_REGISTER_H

#include "date.h"
#include "email.h"

// register data dimensions
#define DIM_ID 4
#define DIM_NAME 16
#define DIM_EMAIL 36
#define DIM_DATE 11
#define DIM_BLOOD_TYPE_STRING 3
#define DIM_NUMBER_OF_DONATIONS 23
#define DIM_RESTRICTION 16

//other parameters
#define DIM_BLOOD_TYPES 9
#define DAYS_TO_WAIT_BEFORE_DONATIONS 90
#define MAX_NUMBER_OF_DONATION 100

typedef enum {
    type_A_p = 0,
    type_A_m,
    type_B_p,
    type_B_m,
    type_0_p,
    type_0_m,
    type_AB_p,
    type_AB_m,
    type_unknown
} bloodType;

typedef struct {
    int id;
    char last_name[DIM_NAME + 1];
    char first_name[DIM_NAME + 1];
    bloodType bloodType;
    char bloodTypeString[DIM_BLOOD_TYPE_STRING + 1];
    char email[DIM_EMAIL + 1];
    int numberOfDonation;
    char dateOfLastDonation[DIM_DATE];
    dateStruct structDateOfLastDonation;
    email_validity_code isCorruptedEmail;
    date_validity_code isCorruptedDoD;
} donorRecord;

int readRegister(const char *, donorRecord *, int, dateStruct);

int countLinesInRegister(const char *, int *);

void printRegister(donorRecord *, int);

void printRegisterHeader(char *);

void printRegisterFooter();

int writeRegister(const char *, donorRecord *, int);

void printRecord(donorRecord, char *);

int searchDonorByBloodType(donorRecord *, int, dateStruct);

donorRecord *addNewDonorToRegister(donorRecord *, int **, dateStruct);

bloodType convertBloodTypeNameToType(const char *);

void printStatistics(const donorRecord *, int, dateStruct);

int modifyDonorRecord(donorRecord *, int, dateStruct);

donorRecord *deleteDonorRecord(donorRecord *, int **);

#endif //BLOOD_DONATION_REGISTER_REGISTER_H
