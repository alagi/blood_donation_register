//
// Created by vargat on 2019. 12. 05..
//
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "util.h"

void usage(char *fileName) {
    printf(ANSI_COLOR_RED"\nblood_donation_register\n\nUsage:%s <filename>\n"ANSI_COLOR_RESET, fileName);
}

void clrscr() {
    system("@cls||clear");
}

int intPow(int base, int exp) {
    int prod = base;
    int i = 1;

    if (exp == 0)
        return 1;

    for (; i < exp; i++)
        prod *= base;

    return prod;
}

int strCompT(const char *str1, const char *str2) {
    int i = 0;

    for (; (str1[i] == str2[i]) && (str1[i] != '\0'); i++);

    if (str1[i] < str2[i])
        return -1;
    else if (str1[i] > str2[i])
        return 1;
    else
        return 0;
}

int isConfirmed(char *externalMessage) {
    char choice;
    char *internalMessage = "Please confirm, ("
                            ANSI_BCOLOR_BLACK"y"ANSI_COLOR_RESET
                            ")es or ("
                            ANSI_BCOLOR_BLACK"n"ANSI_COLOR_RESET
                            ")o: ";

    printf("%s", externalMessage);
    do {
        choice = getCharInput(internalMessage);

        if (choice == 'y')
            return 1;
        if (choice == 'n')
            return 0;

    } while (1);
}

int getStringInput(const char *externalMessage, int sizeOfInput, char *strData) {

    int length = 0;
    char *s = NULL;

    s = malloc(BUFFER_SIZE * sizeof *s);

    if (externalMessage)
        printf("%s", externalMessage);

    while (1) {
        fgets(s, BUFFER_SIZE, stdin);
        if (s[0] == '\n') {
            printf("Line is empty, please enter correct input (string): ");
            continue;
        }
        break;
    }
    for (; length < BUFFER_SIZE; length++) {
        if (s[length] == '\n' || s[length] == '\r' || s[length] == '\0')
            break;
    }
    s[length] = '\0';

    if (length > sizeOfInput) {
        free(s);
        return 0;
    }
    sscanf(s, "%s", strData);
    free(s);
    return length;
}



int getIntInput(char *externalMessage) {
    int validInt;
    char otherChar;
    char s[80];

    if (externalMessage)
        printf("%s ", externalMessage);

    while (1) {
        fgets(s, sizeof(s), stdin);
        if (s[0] == '\n') {
            printf("Line is empty, please enter correct number: ");
            continue;
        }

        if (sscanf(s, "%d%c", &validInt, &otherChar) != 2 || otherChar != '\n') {
            printf("Please, enter only numbers, try again: ");
        } else
            break;
    }
    return validInt;
}

char getCharInput(char *externalMessage) {
    char validChar;
    char otherChar;
    char s[80];

    if (externalMessage)
        printf("%s ", externalMessage);

    while (1) {
        fgets(s, sizeof s, stdin);
        if (s[0] == '\n') {
            printf("Line is empty, please enter correct input (char): ");
            continue;
        }

        if (sscanf(s, "%c%c", &validChar, &otherChar) != 2 || otherChar != '\n') {
            printf("Please, enter only one character, try again: ");
        } else
            break;
    }
    return validChar;
}

int exitProgram() {
    printf(ANSI_COLOR_BLUE"The program exits, GoodBye!!!\n"ANSI_COLOR_RESET);
    sleep(SLEEP);
    clrscr();

    return EXIT_SUCCESS;
}


