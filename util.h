//
// Created by vargat on 2019. 12. 05..
//
#ifndef BLOOD_DONATION_REGISTER_UTIL_H
#define BLOOD_DONATION_REGISTER_UTIL_H

#define SLEEP 3
#define BUFFER_SIZE 256

//defining some colors
#define ANSI_COLOR_RED      "\x1b[31m"
#define ANSI_COLOR_GREEN    "\x1b[32m"
#define ANSI_COLOR_BLUE     "\x1b[34m"
#define ANSI_COLOR_MAGENTA  "\x1b[35m"
#define ANSI_COLOR_RESET    "\x1b[0m"
#define ANSI_BCOLOR_BLACK   "\e[1;30m"
#define ANSI_REDBGN         "\e[41m"
#define ANSI_COLOR_BBLU     "\e[1;34m"

void usage(char *);

void clrscr();

int intPow(int, int);

int strCompT(const char *, const char *);

int isConfirmed(char *);

int getStringInput(const char *, int, char *);

int getIntInput(char *);

char getCharInput(char *);

int exitProgram();

#endif //BLOOD_DONATION_REGISTER_UTIL_H

